package com.example.cockpitsimu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;


import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import com.google.android.things.pio.PeripheralManager;
import com.google.android.things.pio.UartDevice;

import java.io.Console;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.*;

public class MainActivity extends AppCompatActivity {

    // UART Device Name
    private static final String UART_DEVICE_NAME = "COM1";
    private UartDevice mDevice;
    private Toolbar mToolbar;


    public void configureUartFrame(UartDevice uart) throws IOException {
        // Configure the UART port
        uart.setBaudrate(115200);
        uart.setDataSize(8);
        uart.setParity(UartDevice.PARITY_NONE);
        uart.setStopBits(1);
    }


    public static void main(String[] args) {
        PeripheralManager manager = PeripheralManager.getInstance();
        List<String> deviceList = manager.getUartDeviceList();

        if(deviceList.isEmpty()){
            Log.i("***", "No UART port available on this device.");
        } else {
            Log.i("***", "List of available devices: " + deviceList);
        }
    }

    public void setFlowControlEnabled(UartDevice uart, boolean enable) throws IOException {
        if (enable) {
            // Enable hardware flow control
            uart.setHardwareFlowControl(UartDevice.HW_FLOW_CONTROL_AUTO_RTSCTS);
        } else {
            // Disable flow control
            uart.setHardwareFlowControl(UartDevice.HW_FLOW_CONTROL_NONE);
        }
    }

    public void writeUartData(UartDevice uart) throws IOException {
        byte[] buffer = {};
        int count = uart.write(buffer, buffer.length);
        Log.d("****", "Wrote " + count + " bytes to peripheral");
    }

    public void readUartBuffer(UartDevice uart) throws IOException {
        // Maximum amount of data to read at one time
        final int maxCount = 0; // à re-définir
        byte[] buffer = new byte[maxCount];

        int count;
        while ((count = uart.read(buffer, buffer.length)) > 0) {
            Log.d("***", "Read " + count + " bytes from peripheral");
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mDevice != null) {
            try {
                mDevice.close();
                mDevice = null;
                Log.w("*****", "UART device closed");
            } catch (IOException e) {
                Log.w("*****", "Unable to close UART device", e);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Attempt to access the UART device
        try {
            PeripheralManager manager = PeripheralManager.getInstance();
            mDevice = manager.openUartDevice(UART_DEVICE_NAME);
            Log.w("******", "UART device -- OK");

        } catch (IOException e) {
            Log.w("******", "Unable to access UART device", e);
        }

        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final ImageView image_volet = (ImageView) findViewById(R.id.imageVolet);
        SeekBar simpleSeekBar=(SeekBar) findViewById(R.id.simpleSeekBar); // initiate the Seekbar
        final TextView seekBarValue = (TextView)findViewById(R.id.valueSeekBar);
        simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                seekBarValue.setText(String.valueOf(progress));


                if(seekBarValue.getText().equals("0")) {
                    //image volet position 0
                    image_volet.setImageResource(R.mipmap.volet0);
                    Console cons = System.console();
                    String volet0 = "volet0";
                    //if ((cons = System.console())!=null) {

                        System.out.printf("trame volet0");
                    //}
                }
                if(seekBarValue.getText().equals("1")) {
                    //image volet position 1
                    image_volet.setImageResource(R.mipmap.volet1);
                    //afficher dans le terminal la commande LINDA correspondante
                    String volet1 = "volet1";
                    System.out.printf("trame volet1 %s", volet1);
                    Log.i("*****trame :", "volet1 :" +volet1);
                }
                if(seekBarValue.getText().equals("2")) {
                    //image volet position 2
                    image_volet.setImageResource(R.mipmap.volet2);
                    String volet2 = "volet2";
                    System.out.printf("trame volet2 %s", volet2);

                }
                if(seekBarValue.getText().equals("3")) {
                    //image volet position 3
                    image_volet.setImageResource(R.mipmap.volet3);
                }
                if(seekBarValue.getText().equals("4")) {
                    //image volet position 4
                    image_volet.setImageResource(R.mipmap.volet4);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });


        Switch sw_light = (Switch) findViewById(R.id.switch_light);
        sw_light.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled

                    //envoyer information lumière ON au PC
                } else {
                    // The toggle is disabled

                    //envoyer information lumière OFF au PC
                }
            }
        });

        Switch sw_train = (Switch) findViewById(R.id.switch_train);
        sw_train.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled

                    //envoyer information train ON au PC
                } else {
                    // The toggle is disabled

                    //envoyer information train OFF au PC
                }
            }
        });

        Switch sw_frein = (Switch) findViewById(R.id.switch_frein);
        sw_frein.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled

                    //envoyer information frein ON au PC
                } else {
                    // The toggle is disabled

                    //envoyer information frein OFF au PC
                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_base, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        if(id == R.id.goto_freq) {
            Intent intent = new Intent(MainActivity.this, RadioFreqActivity.class);
            startActivity(intent);
            return true;
        }else if(id == R.id.goto_speed) {
            Intent intent = new Intent(MainActivity.this, VitesseActivity.class);
            startActivity(intent);
            return true;
        }else {
            return false;
        }
    }
}

