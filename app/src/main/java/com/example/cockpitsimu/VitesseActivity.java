package com.example.cockpitsimu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class VitesseActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vitesse);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final EditText speed_tosend = findViewById(R.id.edit_speed);
        Button but_plus = findViewById(R.id.but_plus);
        Button but_minus = findViewById(R.id.but_minus);
        Button but_send = findViewById(R.id.but_send);

        but_plus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // Code here executes on main thread after user presses button
                if (speed_tosend.getText().toString().equals("")) {
                    speed_tosend.setText("1");
                }else{
                    int new_speed = Integer.parseInt(speed_tosend.getText().toString())+1;
                    speed_tosend.setText(Integer.toString(new_speed));
                }
            }
        });
        but_minus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // Code here executes on main thread after user presses button
                if (speed_tosend.getText().toString().equals("")) {
                    speed_tosend.setText("0");
                }else if (speed_tosend.getText().toString().equals("0")){
                    //keep 0
                }else{
                    int new_speed = Integer.parseInt(speed_tosend.getText().toString())-1;
                    speed_tosend.setText(Integer.toString(new_speed));
                }
            }
        });


        but_send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // Code here executes on main thread after user presses button
                //envoyer la vitesse au PC

                //rentrer la vitesse recue dans edit text
            }
        });






    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vitesse, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        if(id == R.id.goto_freq) {
            Intent intent = new Intent(VitesseActivity.this, RadioFreqActivity.class);
            startActivity(intent);
            return true;
        }else if(id == R.id.goto_main) {
            Intent intent = new Intent(VitesseActivity.this, MainActivity.class);
            startActivity(intent);
            return true;
        }else {
            return false;
        }
    }
}
